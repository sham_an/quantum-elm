### Quantum Elm
Landing page of student science club named Quantum Elm promotes learning and studying nanotechnology. QE website contains few sections - description about science club, our goals, gallery and achievements
#### Used technologies:
* HTML5
* CSS3
* JavaScript
* jQuery
* Bootstrap4
All pictures (except photos from gallery) come from -> https://unsplash.com and https://www.pexels.com/ edited in Adobe Photoshop.
here you can see live version -> http://quantumelm.frontapp.webd.pro/
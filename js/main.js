$(function() {
    $('#arrow').click(function () {
        $('html, body').animate({ 
            scrollTop: $('#homeAbout').offset().top 
        }, 1200) 
    });

    $(window).scroll(function () {
        $('nav').toggleClass('scrolled', $(this).scrollTop() > 100);
    });
    
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
    
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 1000);
    });

    new Waypoint({
        element: document.getElementById('numbers'),
        handler: function() {
            var options = {
                useEasing: true,
                useGrouping: true,
            };
            
            new CountUp('countMembers', 0, 13, 0, 4, options).start();
            new CountUp('countProjects', 0, 4, 0, 4, options).start();
            new CountUp('countPosters', 0, 3, 0, 4, options).start();
            new CountUp('countWorkshops', 0, 7, 0, 4, options).start();
            this.destroy();
        },
        offset: 150
    });
    
    new WOW().init();
    
});